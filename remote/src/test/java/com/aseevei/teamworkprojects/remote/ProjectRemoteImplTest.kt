package com.aseevei.teamworkprojects.remote

import com.aseevei.teamworkprojects.data.model.ProjectEntity
import com.aseevei.teamworkprojects.remote.mapper.ProjectEntityMapper
import com.aseevei.teamworkprojects.remote.model.ProjectModel
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ProjectRemoteImplTest {

    private lateinit var projectMapper: ProjectEntityMapper
    private lateinit var teamworkService: TeamworkService
    private lateinit var projectRemoteImpl: ProjectRemoteImpl

    @Before
    fun setup() {
        projectMapper = mock()
        teamworkService = mock()
        projectRemoteImpl = ProjectRemoteImpl(teamworkService, projectMapper)
    }

    @Test
    fun getProjectsCompletes() {
        val response = TeamworkService.ProjectResponse()
        response.projects = mutableListOf()
        whenever(teamworkService.getProjects()).thenReturn(Single.just(response))
        val testObserver = projectRemoteImpl.getProjects().test()
        testObserver.assertComplete()
    }

    @Test
    fun getProjectsReturnsData() {
        val response = TeamworkService.ProjectResponse()
        response.projects = mutableListOf(ProjectModel("Name", "Description", "Logo"))
        whenever(teamworkService.getProjects()).thenReturn(Single.just(response))
        val projectEntities = mutableListOf<ProjectEntity>()
        response.projects.forEach { projectEntities.add(projectMapper.mapFromRemote(it)) }

        val testObserver = projectRemoteImpl.getProjects().test()
        testObserver.assertValue(projectEntities)
    }
}
