package com.aseevei.teamworkprojects.remote.mapper

import com.aseevei.teamworkprojects.remote.model.ProjectModel
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ProjectEntityMapperTest {

    private lateinit var projectEntityMapper: ProjectEntityMapper

    @Before
    fun setUp() {
        projectEntityMapper = ProjectEntityMapper()
    }

    @Test
    fun mapFromRemoteMapsData() {
        val projectModel = ProjectModel("Name", "Description", "Logo")
        val projectEntity = projectEntityMapper.mapFromRemote(projectModel)

        assertThat(projectModel.name).isEqualTo(projectEntity.name)
        assertThat(projectModel.description).isEqualTo(projectEntity.description)
        assertThat(projectModel.logo).isEqualTo(projectEntity.logo)
    }
}
