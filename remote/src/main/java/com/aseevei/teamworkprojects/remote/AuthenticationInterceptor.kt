package com.aseevei.teamworkprojects.remote

import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

/**
 * Network Interceptor for adding Basic Authentication to requests.
 */
class AuthenticationInterceptor(private val username: String, private val password: String) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(chain.request().newBuilder()
            .header("Authorization", Credentials.basic(username, password)).build())
    }
}
