package com.aseevei.teamworkprojects.remote

import com.aseevei.teamworkprojects.remote.model.ProjectModel
import com.google.gson.annotations.SerializedName
import io.reactivex.Single
import retrofit2.http.GET

/**
 * Defines the abstract methods used for interacting with the Teamwork API.
 */
interface TeamworkService {

    @GET("projects.json")
    fun getProjects(): Single<ProjectResponse>

    class ProjectResponse {
        @SerializedName("projects")
        lateinit var projects: List<ProjectModel>
    }
}
