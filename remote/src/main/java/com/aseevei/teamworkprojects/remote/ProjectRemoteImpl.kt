package com.aseevei.teamworkprojects.remote

import com.aseevei.teamworkprojects.data.model.ProjectEntity
import com.aseevei.teamworkprojects.data.repository.ProjectRemote
import com.aseevei.teamworkprojects.remote.mapper.ProjectEntityMapper
import io.reactivex.Single
import javax.inject.Inject

/**
 * Remote implementation for retrieving Project instances. This class implements the
 * [ProjectRemote] from the Data layer as it is that layers responsibility for defining the
 * operations in which data store implementation layers can carry out.
 */
class ProjectRemoteImpl @Inject constructor(
    private val teamworkService: TeamworkService,
    private val entityMapper: ProjectEntityMapper
) : ProjectRemote {

    /**
     * Retrieve a list of [ProjectEntity] instances from the [TeamworkService].
     */
    override fun getProjects(): Single<List<ProjectEntity>> {
        return teamworkService.getProjects()
            .map { it.projects.map { listItem -> entityMapper.mapFromRemote(listItem) } }
    }
}
