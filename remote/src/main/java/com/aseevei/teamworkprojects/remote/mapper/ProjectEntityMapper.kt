package com.aseevei.teamworkprojects.remote.mapper

import com.aseevei.teamworkprojects.data.model.ProjectEntity
import com.aseevei.teamworkprojects.remote.model.ProjectModel
import javax.inject.Inject

/**
 * Map a [ProjectModel] to and from a [ProjectEntity] instance when data is moving between
 * this later and the Data layer.
 */
open class ProjectEntityMapper @Inject constructor() : EntityMapper<ProjectModel, ProjectEntity> {

    /**
     * Map an instance of a [ProjectModel] to a [ProjectEntity] model.
     */
    override fun mapFromRemote(type: ProjectModel): ProjectEntity {
        return ProjectEntity(type.name, type.description, type.logo)
    }
}
