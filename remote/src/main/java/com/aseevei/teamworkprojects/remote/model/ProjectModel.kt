package com.aseevei.teamworkprojects.remote.model

import com.google.gson.annotations.SerializedName

/**
 * Representation for a [ProjectModel] fetched from the API.
 */
class ProjectModel(@SerializedName("name") val name: String,
                   @SerializedName("description") val description: String,
                   @SerializedName("logo") val logo: String)
