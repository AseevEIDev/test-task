package com.aseevei.teamworkprojects.presentation.mapper

import com.aseevei.teamworkprojects.domain.model.Project
import com.aseevei.teamworkprojects.presentation.model.ProjectView
import javax.inject.Inject

/**
 * Map a [ProjectView] to and from a [Project] instance when data is moving between
 * this layer and the Domain layer
 */
open class ProjectMapper @Inject constructor(): Mapper<ProjectView, Project> {

    /**
     * Map a [Project] instance to a [ProjectView] instance
     */
    override fun mapToView(type: Project): ProjectView {
        return ProjectView(type.name, type.description, type.logo)
    }


}