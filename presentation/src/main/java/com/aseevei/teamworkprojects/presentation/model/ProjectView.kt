package com.aseevei.teamworkprojects.presentation.model

/**
 * Representation for a [ProjectView] instance for this layers Model representation.
 */
class ProjectView(val name: String, val description: String, val logo: String)
