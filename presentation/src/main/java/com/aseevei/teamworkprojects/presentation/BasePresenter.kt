package com.aseevei.teamworkprojects.presentation

/**
 * Interface class to act as a base for any class that is to take the role of the IPresenter in the
 * Model-BaseView-IPresenter pattern.
 */
interface BasePresenter<in T: BaseView> {

    fun attachView(view: T)

    fun detachView()

    fun stop()
}
