package com.aseevei.teamworkprojects.presentation.list

import com.aseevei.teamworkprojects.presentation.BasePresenter
import com.aseevei.teamworkprojects.presentation.BaseView
import com.aseevei.teamworkprojects.presentation.model.ProjectView


/**
 * Defines a contract of operations between the List Project Presenter and List Project View.
 */
interface ListProjectContract {

    interface View : BaseView {

        fun showProjects(projects: List<ProjectView>)

        fun showErrorState()

        fun showEmptyState()
    }

    interface Presenter : BasePresenter<View> {

        fun getProjects()
    }

}
