package com.aseevei.teamworkprojects.presentation.list

import com.aseevei.teamworkprojects.domain.interactor.browse.GetProjects
import com.aseevei.teamworkprojects.domain.model.Project
import com.aseevei.teamworkprojects.presentation.mapper.ProjectMapper
import com.nhaarman.mockito_kotlin.KArgumentCaptor
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.observers.DisposableSingleObserver
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ListProjectsPresenterTest {

    private lateinit var mockView: ListProjectContract.View
    private lateinit var mockProjectMapper: ProjectMapper
    private lateinit var mockGetProjects: GetProjects

    private lateinit var listProjectPresenter: ListProjectPresenter
    private lateinit var captor: KArgumentCaptor<DisposableSingleObserver<List<Project>>>

    @Before
    fun setup() {
        captor = argumentCaptor()
        mockView = mock()
        mockProjectMapper = mock()
        mockGetProjects = mock()
        listProjectPresenter = ListProjectPresenter(mockGetProjects, mockProjectMapper)
    }

    @Test
    fun getProjectsSuccess() {
        listProjectPresenter.attachView(mockView)
        verify(mockGetProjects).execute(captor.capture(), eq(null))
        captor.firstValue.onSuccess(mutableListOf(Project("Name", "Desc", "Logo")))

        verify(mockView).showProjects(any())
        verifyNoMoreInteractions(mockView)
    }

    @Test
    fun getProjectsSuccess_emptyList() {
        listProjectPresenter.attachView(mockView)
        verify(mockGetProjects).execute(captor.capture(), eq(null))
        captor.firstValue.onSuccess(mutableListOf())

        verify(mockView).showEmptyState()
        verifyNoMoreInteractions(mockView)
    }

    @Test
    fun getProjectsError() {
        listProjectPresenter.attachView(mockView)
        verify(mockGetProjects).execute(captor.capture(), eq(null))
        captor.firstValue.onError(RuntimeException())

        verify(mockView).showErrorState()
        verifyNoMoreInteractions(mockView)
    }

    @Test
    fun stop() {
        listProjectPresenter.stop()
        verify(mockGetProjects).dispose()
    }

    @Test
    fun attachView_isLoading() {
        listProjectPresenter.attachView(mockView)
        listProjectPresenter.attachView(mockView)
        verify(mockGetProjects, times(1)).execute(captor.capture(), eq(null))
    }

    @Test
    fun attachView_dataLoaded() {
        getProjectsSuccess()
        listProjectPresenter.attachView(mockView)
        verify(mockView, times(2)).showProjects(any())
    }

    @Test
    fun detachView() {
        listProjectPresenter.attachView(mockView)
        listProjectPresenter.detachView()
        verify(mockGetProjects).execute(captor.capture(), eq(null))
        captor.firstValue.onError(RuntimeException())

        verifyNoMoreInteractions(mockView)
    }
}
