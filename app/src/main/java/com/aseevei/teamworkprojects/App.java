package com.aseevei.teamworkprojects;

import android.app.Application;
import com.aseevei.teamworkprojects.injection.AppComponent;
import com.aseevei.teamworkprojects.injection.DaggerAppComponent;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

/** Represents an application context of the application. */
public class App extends Application {

    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        setupTimber();
        appComponent = DaggerAppComponent
                .builder()
                .application(this)
                .build();
    }

    private void setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    /**
     * Get application component instance.
     *
     * @return application component for providing dependencies.
     */
    public AppComponent getAppComponent() {
        return appComponent;
    }
}
