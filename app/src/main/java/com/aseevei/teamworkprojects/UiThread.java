package com.aseevei.teamworkprojects;

import com.aseevei.teamworkprojects.domain.executor.PostExecutionThread;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import javax.inject.Inject;
import org.jetbrains.annotations.NotNull;

/**
 * MainThread (UI Thread) implementation based on a {@link Scheduler}
 * which will execute actions on the Android UI thread.
 */
public class UiThread implements PostExecutionThread {

    @Inject
    public UiThread() {
    }

    @NotNull
    @Override
    public Scheduler getScheduler() {
        return AndroidSchedulers.mainThread();
    }
}
