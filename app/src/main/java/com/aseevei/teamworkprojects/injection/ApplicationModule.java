package com.aseevei.teamworkprojects.injection;

import android.app.Application;
import android.content.Context;
import com.aseevei.teamworkprojects.BuildConfig;
import com.aseevei.teamworkprojects.R;
import com.aseevei.teamworkprojects.UiThread;
import com.aseevei.teamworkprojects.cache.PreferencesHelper;
import com.aseevei.teamworkprojects.cache.ProjectCacheImpl;
import com.aseevei.teamworkprojects.cache.db.DbOpenHelper;
import com.aseevei.teamworkprojects.cache.mapper.ProjectEntityMapper;
import com.aseevei.teamworkprojects.data.ProjectDataRepository;
import com.aseevei.teamworkprojects.data.executor.JobExecutor;
import com.aseevei.teamworkprojects.data.mapper.ProjectMapper;
import com.aseevei.teamworkprojects.data.repository.ProjectCache;
import com.aseevei.teamworkprojects.data.repository.ProjectRemote;
import com.aseevei.teamworkprojects.data.source.ProjectDataStoreFactory;
import com.aseevei.teamworkprojects.domain.executor.PostExecutionThread;
import com.aseevei.teamworkprojects.domain.executor.ThreadExecutor;
import com.aseevei.teamworkprojects.domain.repository.ProjectRepository;
import com.aseevei.teamworkprojects.remote.ProjectRemoteImpl;
import com.aseevei.teamworkprojects.remote.ProjectServiceFactory;
import com.aseevei.teamworkprojects.remote.TeamworkService;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

/** A {@link Module} for providing application components. */
@Module
public class ApplicationModule {

    @Provides
    @Singleton
    public Context provideContext(Application application) {
        return application;
    }

    @Provides
    @Singleton
    public PreferencesHelper providePreferencesHelper(Context context) {
        return new PreferencesHelper(context);
    }

    @Provides
    @Singleton
    public PostExecutionThread providePostExecutionThread(UiThread uiThread) {
        return uiThread;
    }

    @Provides
    @Singleton
    public ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides
    @Singleton
    public TeamworkService provideTeamworkService() {
        return ProjectServiceFactory.INSTANCE.makeTeamworkService(BuildConfig.DEBUG, BuildConfig.API_URL);
    }

    @Provides
    @Singleton
    public ProjectRemote provideProjectRemote(TeamworkService service,
                                              com.aseevei.teamworkprojects.remote.mapper.ProjectEntityMapper mapper) {
        return new ProjectRemoteImpl(service, mapper);
    }

    @Provides
    @Singleton
    public ProjectCache provideProjectCache(
            DbOpenHelper dbOpenHelper,
            ProjectEntityMapper entityMapper,
            PreferencesHelper preferencesHelper) {
        return new ProjectCacheImpl(dbOpenHelper, entityMapper, preferencesHelper);
    }

    @Provides
    @Singleton
    public ProjectRepository provideProjectRepository(
            ProjectDataStoreFactory factory,
            ProjectMapper mapper) {
        return new ProjectDataRepository(factory, mapper);
    }
}
