package com.aseevei.teamworkprojects.ui.project.list;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.aseevei.teamworkprojects.R;
import com.aseevei.teamworkprojects.mapper.ProjectMapper;
import com.aseevei.teamworkprojects.presentation.list.ListProjectContract;
import com.aseevei.teamworkprojects.presentation.list.ListProjectPresenter;
import com.aseevei.teamworkprojects.presentation.model.ProjectView;
import com.aseevei.teamworkprojects.ui.project.details.ProjectDetailsFragment;
import com.google.common.collect.FluentIterable;
import java.util.List;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import org.jetbrains.annotations.NotNull;

/** This screen show the list of teamwork projects. */
public class ProjectListFragment extends Fragment implements ListProjectContract.View {

  private static final float SCALE_IN_VALUE = 0.8f;
  @BindView(R.id.recycler)
  RecyclerView recycler;

  @BindView(R.id.progress_loading)
  ProgressBar loadingProgress;

  @BindView(R.id.button_retry)
  Button retryButton;

  @BindView(R.id.text_message)
  TextView messageText;

  private Unbinder unbinder;
  private ProjectRecyclerAdapter adapter;
  private ListProjectPresenter presenter;
  private ProjectMapper mapper = new ProjectMapper();

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_project_list, container, false);
    unbinder = ButterKnife.bind(this, view);
    presenter = ViewModelProviders.of(this).get(ProjectListPresenterHolder.class).getPresenter();
    retryButton.setOnClickListener(ign -> retryLoadProjects());
    setUpRecyclerView();
    presenter.attachView(this);

    return view;
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    presenter.detachView();
    unbinder.unbind();
  }

  @Override
  public void showProjects(@NotNull List<ProjectView> projects) {
    recycler.setVisibility(View.VISIBLE);
    loadingProgress.setVisibility(View.GONE);
    adapter.addProjects(FluentIterable.from(projects).transform(project -> mapper.mapToUIModel(project)).toList());
  }

  @Override
  public void showErrorState() {
    loadingProgress.setVisibility(View.GONE);
    recycler.setVisibility(View.GONE);
    retryButton.setVisibility(View.VISIBLE);
    messageText.setVisibility(View.VISIBLE);
    messageText.setText(R.string.connection_error_message);
  }

  @Override
  public void showEmptyState() {
    loadingProgress.setVisibility(View.GONE);
    recycler.setVisibility(View.GONE);
    retryButton.setVisibility(View.VISIBLE);
    messageText.setVisibility(View.VISIBLE);
    messageText.setText(R.string.no_projects_message);
  }

  private void setUpRecyclerView() {
    Context context = requireContext();
    recycler.setLayoutManager(new LinearLayoutManager(context));
    recycler.setHasFixedSize(true);
    DividerItemDecoration itemDecorator = new DividerItemDecoration(context, DividerItemDecoration.VERTICAL);
    itemDecorator.setDrawable(ContextCompat.getDrawable(context, R.drawable.list_divider));
    recycler.addItemDecoration(itemDecorator);
    adapter = new ProjectRecyclerAdapter();
    adapter.setItemClickListener(project -> {
      Bundle bundle = new Bundle();
      bundle.putSerializable(ProjectDetailsFragment.KEY_PROJECT, project);
      Navigation.findNavController(requireActivity(), R.id.container)
              .navigate(R.id.action_projectListFragment_to_projectDetailsFragment, bundle);
    });
    recycler.setAdapter(new ScaleInAnimationAdapter(adapter, SCALE_IN_VALUE));
  }

  private void retryLoadProjects() {
    retryButton.setVisibility(View.GONE);
    messageText.setVisibility(View.GONE);
    loadingProgress.setVisibility(View.VISIBLE);
    presenter.getProjects();
  }
}
