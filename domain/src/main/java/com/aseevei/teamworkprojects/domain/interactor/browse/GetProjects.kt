package com.aseevei.teamworkprojects.domain.interactor.browse

import com.aseevei.teamworkprojects.domain.executor.PostExecutionThread
import com.aseevei.teamworkprojects.domain.executor.ThreadExecutor
import com.aseevei.teamworkprojects.domain.interactor.SingleUseCase
import com.aseevei.teamworkprojects.domain.model.Project
import com.aseevei.teamworkprojects.domain.repository.ProjectRepository
import io.reactivex.Single
import javax.inject.Inject

/**
 * Use case used for getting a [List] of [Project] instances from the [ProjectRepository]
 */
open class GetProjects @Inject constructor(
    private val projectRepository: ProjectRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : SingleUseCase<List<Project>, Void?>(threadExecutor, postExecutionThread) {

    public override fun buildUseCaseObservable(params: Void?): Single<List<Project>> {
        return projectRepository.getProjects()
    }
}
