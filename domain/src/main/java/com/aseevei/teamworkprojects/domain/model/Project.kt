package com.aseevei.teamworkprojects.domain.model

/**
 * Representation for a [Project] fetched from an external layer data source.
 */
data class Project(val name: String, val description: String, val logo: String)
