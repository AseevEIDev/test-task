package com.aseevei.teamworkprojects.domain.executor

import java.util.concurrent.Executor

interface ThreadExecutor : Executor
