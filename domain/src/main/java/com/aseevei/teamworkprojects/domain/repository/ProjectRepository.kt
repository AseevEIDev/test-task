package com.aseevei.teamworkprojects.domain.repository

import com.aseevei.teamworkprojects.domain.model.Project
import io.reactivex.Completable
import io.reactivex.Single

/**
 * Interface defining methods for how the data layer can pass data to and from the Domain layer.
 */
interface ProjectRepository {

    fun clearProjects(): Completable

    fun saveProjects(projects: List<Project>): Completable

    fun getProjects(): Single<List<Project>>
}