package com.aseevei.teamworkprojects.data.source

import com.aseevei.teamworkprojects.data.model.ProjectEntity
import com.aseevei.teamworkprojects.data.repository.ProjectCache
import com.aseevei.teamworkprojects.data.repository.ProjectDataStore
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Implementation of the [ProjectDataStore] interface to provide a means of communicating
 * with the local data source
 */
open class ProjectCacheDataStore @Inject constructor(private val projectCache: ProjectCache) :
    ProjectDataStore {

    /**
     * Clear all Projects from the cache
     */
    override fun clearProjects(): Completable {
        return projectCache.clearProjects()
    }

    /**
     * Save a given [List] of [ProjectEntity] instances to the cache
     */
    override fun saveProjects(projects: List<ProjectEntity>): Completable {
        return projectCache.saveProjects(projects)
            .doOnComplete { projectCache.setLastCacheTime(System.currentTimeMillis()) }
    }

    /**
     * Retrieve a list of [ProjectEntity] instance from the cache
     */
    override fun getProjects(): Single<List<ProjectEntity>> {
        return projectCache.getProjects()
    }
}
