package com.aseevei.teamworkprojects.data.source

import com.aseevei.teamworkprojects.data.repository.ProjectCache
import com.aseevei.teamworkprojects.data.repository.ProjectDataStore
import javax.inject.Inject

/**
 * Create an instance of a [ProjectDataStore].
 */
open class ProjectDataStoreFactory @Inject constructor(
    private val projectCache: ProjectCache,
    private val projectCacheDataStore: ProjectCacheDataStore,
    private val projectRemoteDataStore: ProjectRemoteDataStore
) {

    /**
     * Returns a DataStore based on whether or not there is content in the cache and the cache
     * has not expired
     */
    open fun retrieveDataStore(): ProjectDataStore {
        if (projectCache.isCached() && !projectCache.isExpired()) {
            return retrieveCacheDataStore()
        }
        return retrieveRemoteDataStore()
    }

    /**
     * Return an instance of the Remote Data Store
     */
    open fun retrieveCacheDataStore(): ProjectDataStore {
        return projectCacheDataStore
    }

    /**
     * Return an instance of the Cache Data Store
     */
    open fun retrieveRemoteDataStore(): ProjectDataStore {
        return projectRemoteDataStore
    }
}
