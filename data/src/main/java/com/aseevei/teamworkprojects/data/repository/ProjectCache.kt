package com.aseevei.teamworkprojects.data.repository

import com.aseevei.teamworkprojects.data.model.ProjectEntity
import io.reactivex.Completable
import io.reactivex.Single

/**
 * Interface defining methods for the caching of Projects. This is to be implemented by the
 * cache layer, using this interface as a way of communicating.
 */
interface ProjectCache {

    /**
     * Clear all Projects from the cache
     */
    fun clearProjects(): Completable

    /**
     * Save a given list of Projects to the cache
     */
    fun saveProjects(projects: List<ProjectEntity>): Completable

    /**
     * Retrieve a list of Projects, from the cache
     */
    fun getProjects(): Single<List<ProjectEntity>>

    /**
     * Checks if an element exists in the cache.
     *
     * @return true if the element is cached, otherwise false.
     */
    fun isCached(): Boolean

    /**
     * Set the last time, when element was exists. Used for expiring cache after some period of time.
     *
     * @param lastCache - last time in ms.
     */
    fun setLastCacheTime(lastCache: Long)

    /**
     * Checks if the cache is expired.
     *
     * @return true, the cache is expired, otherwise false.
     */
    fun isExpired(): Boolean

}