package com.aseevei.teamworkprojects.data.repository

import com.aseevei.teamworkprojects.data.model.ProjectEntity
import io.reactivex.Single

/**
 * Interface defining methods for the caching of Projects. This is to be implemented by the
 * cache layer, using this interface as a way of communicating.
 */
interface ProjectRemote {

    /**
     * Retrieve a list of Projects from the cache.
     */
    fun getProjects(): Single<List<ProjectEntity>>

}