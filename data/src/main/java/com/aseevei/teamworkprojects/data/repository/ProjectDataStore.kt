package com.aseevei.teamworkprojects.data.repository

import com.aseevei.teamworkprojects.data.model.ProjectEntity
import io.reactivex.Completable
import io.reactivex.Single

/**
 * Interface defining methods for the data operations related to Projects.
 * This is to be implemented by external data source layers, setting the requirements for the
 * operations that need to be implemented.
 */
interface ProjectDataStore {

    fun clearProjects(): Completable

    fun saveProjects(projects: List<ProjectEntity>): Completable

    fun getProjects(): Single<List<ProjectEntity>>
}
