package com.aseevei.teamworkprojects.data.source

import com.aseevei.teamworkprojects.data.repository.ProjectCache
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ProjectDataStoreFactoryTest {

    private lateinit var projectDataStoreFactory: ProjectDataStoreFactory
    private lateinit var projectCache: ProjectCache
    private lateinit var projectCacheDataStore: ProjectCacheDataStore
    private lateinit var projectRemoteDataStore: ProjectRemoteDataStore

    @Before
    fun setUp() {
        projectCache = mock()
        projectCacheDataStore = mock()
        projectRemoteDataStore = mock()
        projectDataStoreFactory = ProjectDataStoreFactory(
            projectCache,
            projectCacheDataStore, projectRemoteDataStore
        )
    }

    @Test
    fun retrieveDataStoreWhenNotCachedReturnsRemoteDataStore() {
        setProjectCacheIsCached(false)
        val projectDataStore = projectDataStoreFactory.retrieveDataStore()
        assert(projectDataStore is ProjectRemoteDataStore)
    }

    @Test
    fun retrieveDataStoreWhenCacheExpiredReturnsRemoteDataStore() {
        setProjectCacheIsCached(true)
        setProjectCacheIsExpired(true)
        val projectDataStore = projectDataStoreFactory.retrieveDataStore()
        assert(projectDataStore is ProjectRemoteDataStore)
    }

    @Test
    fun retrieveDataStoreReturnsCacheDataStore() {
        setProjectCacheIsCached(true)
        setProjectCacheIsExpired(false)
        val projectDataStore = projectDataStoreFactory.retrieveDataStore()
        assert(projectDataStore is ProjectCacheDataStore)
    }

    @Test
    fun retrieveRemoteDataStoreReturnsRemoteDataStore() {
        val projectDataStore = projectDataStoreFactory.retrieveRemoteDataStore()
        assert(projectDataStore is ProjectRemoteDataStore)
    }

    @Test
    fun retrieveCacheDataStoreReturnsCacheDataStore() {
        val projectDataStore = projectDataStoreFactory.retrieveCacheDataStore()
        assert(projectDataStore is ProjectCacheDataStore)
    }

    private fun setProjectCacheIsCached(isCached: Boolean) {
        whenever(projectCache.isCached()).thenReturn(isCached)
    }

    private fun setProjectCacheIsExpired(isExpired: Boolean) {
        whenever(projectCache.isExpired()).thenReturn(isExpired)
    }
}
