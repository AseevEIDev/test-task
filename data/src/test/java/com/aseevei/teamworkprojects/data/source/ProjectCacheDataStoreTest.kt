package com.aseevei.teamworkprojects.data.source

import com.aseevei.teamworkprojects.data.repository.ProjectCache
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ProjectCacheDataStoreTest {

    private lateinit var projectCacheDataStore: ProjectCacheDataStore
    private lateinit var projectCache: ProjectCache

    @Before
    fun setUp() {
        projectCache = mock()
        projectCacheDataStore = ProjectCacheDataStore(projectCache)
    }

    @Test
    fun clearProjectCompletes() {
        whenever(projectCache.clearProjects()).thenReturn(Completable.complete())
        val testObserver = projectCacheDataStore.clearProjects().test()
        testObserver.assertComplete()
    }

    @Test
    fun saveProjectCompletes() {
        whenever(projectCache.saveProjects(any())).thenReturn(Completable.complete())
        val testObserver = projectCacheDataStore.saveProjects(mutableListOf()).test()
        testObserver.assertComplete()
    }

    @Test
    fun getProjectCompletes() {
        whenever(projectCache.getProjects()).thenReturn(Single.just(mutableListOf()))
        val testObserver = projectCacheDataStore.getProjects().test()
        testObserver.assertComplete()
    }
}
