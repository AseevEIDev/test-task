package com.aseevei.teamworkprojects.data

import com.aseevei.teamworkprojects.data.mapper.ProjectMapper
import com.aseevei.teamworkprojects.data.model.ProjectEntity
import com.aseevei.teamworkprojects.data.repository.ProjectDataStore
import com.aseevei.teamworkprojects.data.source.ProjectCacheDataStore
import com.aseevei.teamworkprojects.data.source.ProjectDataStoreFactory
import com.aseevei.teamworkprojects.data.source.ProjectRemoteDataStore
import com.aseevei.teamworkprojects.domain.model.Project
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ProjectDataRepositoryTest {

    private lateinit var projectDataRepository: ProjectDataRepository
    private lateinit var projectDataStoreFactory: ProjectDataStoreFactory
    private lateinit var projectMapper: ProjectMapper
    private lateinit var projectCacheDataStore: ProjectCacheDataStore
    private lateinit var projectRemoteDataStore: ProjectRemoteDataStore

    @Before
    fun setUp() {
        projectDataStoreFactory = mock()
        projectMapper = mock()
        projectCacheDataStore = mock()
        projectRemoteDataStore = mock()
        projectDataRepository = ProjectDataRepository(projectDataStoreFactory, projectMapper)
        whenever(projectDataStoreFactory.retrieveCacheDataStore()).thenReturn(projectCacheDataStore)
        whenever(projectDataStoreFactory.retrieveRemoteDataStore()).thenReturn(projectCacheDataStore)
    }

    @Test
    fun clearProjectsCompletes() {
        setProjectCacheClearProjects(Completable.complete())
        val testObserver = projectDataRepository.clearProjects().test()
        testObserver.assertComplete()
    }

    @Test
    fun clearProjectsCallsCacheDataStore() {
        setProjectCacheClearProjects(Completable.complete())
        projectDataRepository.clearProjects().test()
        verify(projectCacheDataStore).clearProjects()
    }

    @Test
    fun clearProjectsNeverCallsRemoteDataStore() {
        setProjectCacheClearProjects(Completable.complete())
        projectDataRepository.clearProjects().test()
        verify(projectRemoteDataStore, never()).clearProjects()
    }

    @Test
    fun saveProjectsCompletes() {
        setProjectsCacheSaveProjects(Completable.complete())
        val testObserver = projectDataRepository.saveProjects(mutableListOf()).test()
        testObserver.assertComplete()
    }

    @Test
    fun saveProjectsCallsCacheDataStore() {
        setProjectsCacheSaveProjects(Completable.complete())
        projectDataRepository.saveProjects(mutableListOf()).test()
        verify(projectCacheDataStore).saveProjects(any())
    }

    @Test
    fun saveProjectsNeverCallsRemoteDataStore() {
        setProjectsCacheSaveProjects(Completable.complete())
        projectDataRepository.saveProjects(mutableListOf()).test()
        verify(projectRemoteDataStore, never()).saveProjects(any())
    }

    @Test
    fun getProjectsCompletes() {
        setProjectDataStoreFactoryRetrieveDataStore(projectCacheDataStore)
        setProjectCacheDataStoreGetProjects(Single.just(mutableListOf()))
        val testObserver = projectDataRepository.getProjects().test()
        testObserver.assertComplete()
    }

    @Test
    fun getProjectsReturnsData() {
        setProjectDataStoreFactoryRetrieveDataStore(projectCacheDataStore)
        val projects = mutableListOf(Project("Name", "Description", "Logo"))
        val projectEntities = mutableListOf(ProjectEntity("NameEntity", "DescriptionEntity", "LogoEntity"))
        projects.forEachIndexed { index, project ->
            whenever(projectMapper.mapFromEntity(projectEntities[index])).thenReturn(project)
        }
        setProjectCacheDataStoreGetProjects(Single.just(projectEntities))
        val testObserver = projectDataRepository.getProjects().test()
        testObserver.assertValue(projects)
    }

    @Test
    fun getProjectsSavesProjectsWhenFromCacheDataStore() {
        setProjectDataStoreFactoryRetrieveDataStore(projectCacheDataStore)
        setProjectsCacheSaveProjects(Completable.complete())
        projectDataRepository.saveProjects(mutableListOf()).test()
        verify(projectCacheDataStore).saveProjects(any())
    }

    @Test
    fun getProjectsNeverSavesProjectsWhenFromRemoteDataStore() {
        setProjectDataStoreFactoryRetrieveDataStore(projectRemoteDataStore)
        setProjectsCacheSaveProjects(Completable.complete())
        projectDataRepository.saveProjects(mutableListOf()).test()
        verify(projectRemoteDataStore, never()).saveProjects(any())
    }

    private fun setProjectsCacheSaveProjects(completable: Completable) {
        whenever(projectCacheDataStore.saveProjects(any())).thenReturn(completable)
    }

    private fun setProjectCacheDataStoreGetProjects(single: Single<List<ProjectEntity>>) {
        whenever(projectCacheDataStore.getProjects()).thenReturn(single)
    }

    private fun setProjectCacheClearProjects(completable: Completable) {
        whenever(projectCacheDataStore.clearProjects()).thenReturn(completable)
    }

    private fun setProjectDataStoreFactoryRetrieveDataStore(dataStore: ProjectDataStore) {
        whenever(projectDataStoreFactory.retrieveDataStore()).thenReturn(dataStore)
    }
}
