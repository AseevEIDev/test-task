package com.aseevei.teamworkprojects.cache

import android.content.ContentValues
import com.aseevei.teamworkprojects.cache.db.DbOpenHelper
import com.aseevei.teamworkprojects.cache.mapper.ProjectEntityMapper
import com.aseevei.teamworkprojects.cache.model.CachedProject
import com.aseevei.teamworkprojects.data.model.ProjectEntity
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [21])
class ProjectCacheImplTest {

    private var entityMapper = ProjectEntityMapper()
    private var preferencesHelper = PreferencesHelper(RuntimeEnvironment.application)

    private val databaseHelper = ProjectCacheImpl(DbOpenHelper(RuntimeEnvironment.application),
        entityMapper, preferencesHelper)

    @Before
    fun setup() {
        databaseHelper.database.rawQuery("DELETE FROM " + DbOpenHelper.TABLE_NAME, null)
    }

    @Test
    fun clearTablesCompletes() {
        val testObserver = databaseHelper.clearProjects().test()
        testObserver.assertComplete()
    }

    @Test
    fun saveProjectsCompletes() {
        val testObserver = databaseHelper.saveProjects(mutableListOf()).test()
        testObserver.assertComplete()
    }

    @Test
    fun saveProjectsSavesData() {
        databaseHelper.saveProjects(mutableListOf(ProjectEntity("Name", "Desc", "Logo"))).test()
        checkNumRowsInProjectsTable(1)
    }

    @Test
    fun getProjectsCompletes() {
        val testObserver = databaseHelper.getProjects().test()
        testObserver.assertComplete()
    }

    @Test
    fun getProjectsReturnsData() {
        val projectEntities = mutableListOf(ProjectEntity("Name", "Desc", "Logo"))
        val cachedProjects = mutableListOf<CachedProject>()
        projectEntities.forEach { cachedProjects.add(entityMapper.mapToCached(it)) }
        insertProjects(cachedProjects)

        val testObserver = databaseHelper.getProjects().test()
        testObserver.assertValue(projectEntities)
    }

    private fun insertProjects(cachedProjects: List<CachedProject>) {
        val database = databaseHelper.database
        cachedProjects.forEach {
            val values = ContentValues()
            values.put(DbOpenHelper.COLUMN_NAME, it.name)
            values.put(DbOpenHelper.COLUMN_DESCRIPTION, it.description)
            values.put(DbOpenHelper.COLUMN_LOGO, it.logo)
            database.insertOrThrow(DbOpenHelper.TABLE_NAME, null, values)
        }
    }

    private fun checkNumRowsInProjectsTable(expectedRows: Int) {
        val cursor = databaseHelper.database.query(DbOpenHelper.TABLE_NAME,
                null, null, null, null, null, null)
        cursor.moveToFirst()
        val numberOfRows = cursor.count
        cursor.close()
        assertThat(expectedRows).isEqualTo(numberOfRows)
    }
}
